MAP API -> AIzaSyC6plIzXt8kheiDprJUVffh5Ho-FitKAdo
paquete -> com.example.mance
SHA-1 -> F2:13:89:C2:78:A9:78:15:93:A3:44:3F:00:EB:2F:99:D3:F4:84:81
## use camera to take pictures
[link](https://developer.android.com/training/camera/photobasics?hl=es-419#TaskManifest)

## modelo de datos de Manec (firestore)
ejemplo de estructura -> [link](https://github.com/firebase/quickstart-android/blob/master/firestore/app/src/main/java/com/google/firebase/example/fireeats/kotlin/MainFragment.kt)
- Marcadores -> Usuarios
- Usuarios, Marcadores
- Usuarios -> Marcadores
- Usuarios |__> Marcadores (ESCOGIDO)

## final sprint
- MARKERMAP: q haga zoom al clickar en un marcador DONE
- APP: añadir icono a la app y actualizar el nombre DONE
- MENU: agregar un menu para navegar a tu perfil, y borrar el icono de lista 
- MARKERMAP: mostrar todos los cuentos disponibles de una ciudad DONE
- HOME: crear una pagina de inicio donde te pida crear un cuento o navegar por tu ciudad
- shared preferences: guardar datos permanentes, como ciudad actual, login y password DONE
- MARKERS:  como ciudad actual en prefered shared
- MARKERS: agregar pagina de visualizacio de cuentos
- MARKERS: agregar la funcionalidad de poder borrar markers
- LIST: cambiar la lista por una que tenga todos los cuentos
