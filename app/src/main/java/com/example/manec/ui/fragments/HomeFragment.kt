package com.example.manec.ui.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.example.manec.R
import com.example.manec.databinding.FragmentHomeBinding
import com.example.manec.viewModel.MapViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

const val REQUEST_CODE_LOCATION = 100

class HomeFragment: Fragment(R.layout.fragment_home),OnMapReadyCallback {
    val vm: MapViewModel by activityViewModels()
    lateinit var map: GoogleMap
    private lateinit var binding: FragmentHomeBinding

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView =  inflater.inflate(R.layout.fragment_home, container, false)
        createMap()
        return rootView
    }

    @SuppressLint("VisibleForTests", "MissingPermission")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentHomeBinding.bind(view)
        // listeners
        binding.btHomeList.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToMarkerListFragment2()
            Navigation.findNavController(view).navigate(action)
        }

        binding.btHomeProfile.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToProfileFragment()
            Navigation.findNavController(view).navigate(action)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        enableLocation()
        updateDDBBMarkers()
        // listeners
        map.setOnMapLongClickListener {
            addMarkers()
            createMarker(it.latitude,it.longitude)
        }
        map.setOnInfoWindowClickListener {
            val action : NavDirections
            if(vm.currentMarker?.id == it.id) {
                action = HomeFragmentDirections.actionHomeFragmentToCreateMarkerFragment(it.position)
                requireView().findNavController().navigate(action)
            } else {
                action = HomeFragmentDirections.actionHomeFragmentToMarkerFragment2(it.position,-1)
                requireView().findNavController().navigate(action)
            }
        }
        map.setOnMarkerClickListener {
            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(it.position, 18f), 2000, null)
            it.showInfoWindow()
            true
        }
    }

    private fun addMarkers() {
        map.clear()
        vm.getAllMarkers().value?.forEach {
            val marker = MarkerOptions()
                .position(LatLng(
                        it.position?.get("latitude")!!,
                        it.position?.get("longitude")!!
                    ))
                .title(it.title)
            map.addMarker(marker)
        }
    }

    private fun updateDDBBMarkers() {
        map.clear()
        vm.updateMarkers().observe(viewLifecycleOwner) {
            it.forEach {
                val marker = MarkerOptions()
                    .position(
                        LatLng(
                            it.position?.get("latitude")!!,
                            it.position?.get("longitude")!!
                        )
                    )
                    .title(it.title)
                map.addMarker(marker)
            }
        }

    }

    // map functions
    fun createMap(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    fun createMarker(latitute:Double,longitude:Double){
        val coordinates = LatLng(latitute,longitude)
        val myMarker = MarkerOptions().position(coordinates).title("MY NEW ANECDOTE")
        vm.currentMarker = map.addMarker(myMarker)
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(coordinates, 18f),
            1000, null)
    }
    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }


}