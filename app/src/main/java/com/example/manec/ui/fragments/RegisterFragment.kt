package com.example.manec.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.manec.R
import com.example.manec.databinding.FragmentRegisterBinding
import com.example.manec.utils.FirestoreVars
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class RegisterFragment:Fragment(R.layout.fragment_register) {
    lateinit var binding:FragmentRegisterBinding
    val db = FirebaseFirestore.getInstance()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentRegisterBinding.bind(view)
        val registerBtn = binding.btRegisterCreate
        // listerners
        registerBtn.setOnClickListener {
            val email = binding.etRegisterEmail.text.toString()
            val password = binding.etRegisterPassword.text.toString()
            val name = binding.etRegisterName.text.toString()
            val birthDate = binding.etRegisterBirth.text.toString()
            FirebaseAuth.getInstance().
            createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        Toast.makeText(context,"welcome new friend", Toast.LENGTH_LONG).show()
                        // save in remote
                        db.collection(FirestoreVars.collection).document(email).set(
                            hashMapOf(
                                "name" to name,
                                "birth_date" to birthDate,
                            )
                        )
                        // navigate
                        val action = RegisterFragmentDirections.actionRegisterFragmentToLoginFragment()
                        Navigation.findNavController(view).navigate(action)
                    }
                    else{
                        Toast.makeText(context,it.exception?.message, Toast.LENGTH_LONG).show()
                    }
                }
        }
    }
}