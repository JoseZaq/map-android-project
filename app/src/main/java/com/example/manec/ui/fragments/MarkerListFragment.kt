package com.example.manec.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.manec.R
import com.example.manec.ui.adapters.UserMarkerAdapter
import com.example.manec.databinding.FragmentMarkerListBinding
import com.example.manec.ui.adapters.MarkerAdapter
import com.example.manec.viewModel.MapViewModel

class MarkerListFragment: Fragment(R.layout.fragment_marker_list) {
    private val vm: MapViewModel by activityViewModels()
    private lateinit var binding: FragmentMarkerListBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMarkerListBinding.bind(view)
        //

        val adapter = MarkerAdapter()
        binding.rvMarkers.adapter = adapter
        binding.rvMarkers.apply {
            addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        }

        vm.updateMarkers().observe(viewLifecycleOwner, { adapter.setMarkers(it) })

    }
}