package com.example.manec.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.example.manec.R
import com.example.manec.databinding.FragmentLoginBinding
import com.google.firebase.auth.FirebaseAuth

val SHARED_PREFERENCES_FILE = "UserCredentials"

class LoginFragment: Fragment(R.layout.fragment_login) {
    private lateinit var binding: FragmentLoginBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentLoginBinding.bind(view)
        val registerBtn = binding.tvLoginRegister
        val loginBtn = binding.btLoginCreate
        // listeners
        registerBtn.setOnClickListener {
            val action = LoginFragmentDirections.actionLoginFragmentToRegisterFragment()
            view.findNavController().navigate(action)
        }
        loginBtn.setOnClickListener {
            // RESTORE IT !!
            val email = binding.etLoginEmail.text.toString()
            val password = binding.etLoginPassword.text.toString()
            // TEMP
//            val email = "admin@manec.com"
//            val password = "123456"
            saveCredentials()
            FirebaseAuth.getInstance().
            signInWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        Toast.makeText(context,"welcome!",Toast.LENGTH_LONG).show()
                        val action = LoginFragmentDirections.actionLoginFragmentToHomeFragment()
                        Navigation.findNavController(view).navigate(action)
                    }
                    else{
                        Toast.makeText(context,it.exception?.message,Toast.LENGTH_LONG).show()
                    }
                }
        }
    }

    private fun saveCredentials() {
        val email = binding.etLoginEmail.text.toString()
        val password = binding.etLoginPassword.text.toString()
        val sharedPref = activity?.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE)
        val editor = sharedPref!!.edit()
        editor.apply {
            putString("EMAIL", email)
            putString("PASSWORD", password)
        }.apply()
    }

}