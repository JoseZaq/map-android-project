package com.example.manec.ui.adapters

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.manec.R
import com.example.manec.ui.fragments.ProfileFragmentDirections
import com.example.manec.ui.models.Marker
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class UserMarkerAdapter:RecyclerView.Adapter<UserMarkerAdapter.UserMarkerAdapterViewHolder>() {
    private var markers = mutableListOf<Marker>()
    class UserMarkerAdapterViewHolder(view : View): RecyclerView.ViewHolder(view){
        var img: ImageView
        var tvText: TextView
        init{
            img = view.findViewById(R.id.im_marker_icon)
            tvText = view.findViewById(R.id.tv_marker_name)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserMarkerAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_marker,parent,false)
        return UserMarkerAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holderUser: UserMarkerAdapterViewHolder, position: Int) {
        // load fire-storage image and set image
        if(markers[position].icon != null) {
            val storage = FirebaseStorage.getInstance().reference.child(markers[position].icon!!)
            val localFile = File.createTempFile("temp", "jpeg")
            storage.getFile(localFile).addOnSuccessListener {
                val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                holderUser.img.setImageBitmap(bitmap)

            }.addOnFailureListener {
//            Toast.makeText(requireContext(), "Error downloading image!", Toast.LENGTH_SHORT)
//                .show()
            }
        }
        //
        holderUser.tvText.text = markers[position].title
        holderUser.itemView.setOnClickListener {
            val action = ProfileFragmentDirections.actionProfileFragmentToMarkerFragment(position)
            Navigation.findNavController(it).navigate(action)
        }

    }
    override fun getItemCount(): Int {
        return markers.size
    }


    // getters and setters
    @SuppressLint("NotifyDataSetChanged")
    fun setMarkers(markers: MutableList<Marker>){
        this.markers= markers
        notifyDataSetChanged()
    }
}