package com.example.manec.ui.fragments

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.manec.R
import com.example.manec.databinding.FragmentCreateMarkerBinding
import androidx.core.app.ActivityCompat.startActivityForResult

import android.content.pm.PackageManager
import android.net.Uri
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.checkSelfPermission
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.manec.ui.models.Marker
import com.example.manec.utils.FirestoreVars
import com.example.manec.utils.PhoneResources
import com.example.manec.utils.Utils
import com.example.manec.viewModel.MapViewModel
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.io.ByteArrayOutputStream


class CreateMarkerFragment: Fragment(R.layout.fragment_create_marker){
    private val MY_CAMERA_PERMISSION_CODE = 100
    val vm: MapViewModel by activityViewModels()
    lateinit var binding: FragmentCreateMarkerBinding
    var imageUri: Uri? = null

    val db = FirebaseFirestore.getInstance()
    val userEmail = FirebaseAuth.getInstance().currentUser!!.email

    var resultLauncher: ActivityResultLauncher<Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                val data: Intent? = result.data
                if (data != null) {
                    imageUri = data.data!!
                    binding.ibPicture.setImageURI(imageUri)
                }
            }
        }

    var onSuccessUpload = fun () {
        binding.ibPicture.setImageURI(null)
        Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
    }

    var onFailureUpload = fun () {
        Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCreateMarkerBinding.bind(view)
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(requireActivity(), Array(1){
                Manifest.permission.CAMERA
            }, 100)
        }
        //vars
        val currentLocation:LatLng = arguments?.get("location") as LatLng
        // listeners
        binding.ibPicture.setOnClickListener {
            var intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, MY_CAMERA_PERMISSION_CODE)
        }

        binding.btPicture.setOnClickListener { PhoneResources.selectImage(resultLauncher) }
        binding.btCreatorCreate.setOnClickListener {
            if(binding.etCreatorName.text.toString() == "" && binding.etAnecdote.text.toString() == "") {
                Toast.makeText(context,"amazing anecdote!. But don't get it, write more pleease!",Toast.LENGTH_SHORT).show()
            }else {
                val marker = MarkerOptions()
                    .position(currentLocation)
                    .title(binding.etCreatorName.text.toString())
                // add to remote
                val fileName = if(imageUri != null ) FirestoreVars.uploadToFireStorage(imageUri!!, onSuccessUpload, onFailureUpload) else null
                db.collection(FirestoreVars.collection).document(userEmail!!)
                    .collection(FirestoreVars.subColection).document().set(
                        hashMapOf(
                            "title" to marker.title,
                            "position" to marker.position,
                            "anecdote" to binding.etAnecdote.text.toString(),
                            "icon" to fileName))
                // add to local
//                vm.addMarker(marker)
                val action =
                    CreateMarkerFragmentDirections.actionCreateMarkerFragmentToMarkerListFragment()
                Navigation.findNavController(view).navigate(action)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            val image = (data?.extras?.get("data") as Bitmap)
            imageUri = Utils.getImageUriFromBitmap(requireContext(), image)
            binding.ibPicture.setImageURI(imageUri)
        }

    }


}