package com.example.manec.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.manec.R
import com.example.manec.databinding.FragmentProfileBinding
import com.example.manec.ui.adapters.UserMarkerAdapter
import com.example.manec.viewModel.MapViewModel
import com.google.firebase.auth.FirebaseAuth

class ProfileFragment: Fragment(R.layout.fragment_profile) {
    private val vm: MapViewModel by activityViewModels()
    lateinit var binding: FragmentProfileBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentProfileBinding.bind(view)
        // views
        val adapter = UserMarkerAdapter()
        binding.rvProfile.adapter = adapter
        binding.rvProfile.apply {
            addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        }

        vm.updateUserMarkers().observe(viewLifecycleOwner, { adapter.setMarkers(it) })
        // listeners
        binding.btLogOut.setOnClickListener {
            FirebaseAuth.getInstance()
                .signOut().also {
                    clearSharedPreferences()
                    Toast.makeText(context,"see u soon!",Toast.LENGTH_SHORT).show()
                    val action = ProfileFragmentDirections.actionProfileFragmentToLoginFragment()
                    findNavController().navigate(action)
                }
        }
    }

    private fun clearSharedPreferences() {
        val sharedPref = activity?.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE)
        val editor = sharedPref!!.edit()
        editor.apply {
            putString("EMAIL", null)
            putString("PASSWORD", null)
        }.apply()
    }
}