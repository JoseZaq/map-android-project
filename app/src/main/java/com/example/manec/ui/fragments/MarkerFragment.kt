package com.example.manec.ui.fragments

import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.manec.R
import com.example.manec.databinding.FragmentMarkerBinding
import com.example.manec.ui.models.Marker
import com.example.manec.viewModel.MapViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.storage.FirebaseStorage
import java.io.File


class MarkerFragment: Fragment(R.layout.fragment_marker) {
    private lateinit var bind : FragmentMarkerBinding
    private val vm : MapViewModel by activityViewModels()
    private var markerLocation: LatLng? = null // arriving from home
    private var markerPosition: Int? = null // arriving from list
    private var marker : Marker? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bind = FragmentMarkerBinding.bind(view)
        markerLocation = arguments?.getParcelable<LatLng>("position")
        markerPosition = arguments?.getInt("index")
        markerLocation?.let { marker =  vm.getMarkerByPosition(it) }
        markerPosition?.let {
            if(markerPosition != -1 )
                marker =  vm.getMarkerByIndex(it)
        }
        // load fire-storage image and set image
        val localFile = File.createTempFile("temp", "jpeg")
        if(marker?.icon != null) {
            FirebaseStorage.getInstance().reference.child(marker?.icon!!).getFile(localFile)
                .addOnSuccessListener {
                    val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                    val drawable = BitmapDrawable(resources, bitmap)
                    drawable.setAlpha(50)
                    drawable.gravity  = Gravity.FILL
                    view.background = drawable
                }
        }
        // set views
        bind.tvMarkerAnecdote.text = marker?.anecdote ?: ""
        bind.tvMarkerTitle.text = marker?.title ?: ""
    }
}