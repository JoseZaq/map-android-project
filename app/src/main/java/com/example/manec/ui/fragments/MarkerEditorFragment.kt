package com.example.manec.ui.fragments

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.manec.R
import com.example.manec.databinding.FragmentMarkerEditorBinding
import com.example.manec.utils.FirestoreVars
import com.example.manec.utils.PhoneResources
import com.example.manec.utils.Utils
import com.example.manec.viewModel.MapViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class MarkerEditorFragment: Fragment(R.layout.fragment_marker_editor){
    private val MY_CAMERA_PERMISSION_CODE = 100
    private val vm: MapViewModel by activityViewModels()
    private val db = FirebaseFirestore.getInstance()
    private val userEmail = FirebaseAuth.getInstance().currentUser!!.email
    lateinit var binding: FragmentMarkerEditorBinding
    var imageUri: Uri? = null
    val localFile = File.createTempFile("temp", "jpeg")
    var position = 0
    var id = ""

    var resultLauncher: ActivityResultLauncher<Intent> = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                imageUri = data.data!!
                binding.ibMarkerImage.setImageURI(imageUri)
            }
        }
    }

    var onSuccessUpload = fun () {
        binding.ibMarkerImage.setImageURI(null)
        Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
    }

    var onFailureUpload = fun () {
        Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMarkerEditorBinding.bind(view)
        position = arguments?.getInt("position") ?: 0
        id = arguments?.getString("id") ?: ""
        // vars
        val marker = vm.getUserMarkerByIndex(position)
        // load fire-storage image and set image
        if(marker.icon != null) {
            FirebaseStorage.getInstance().reference.child(marker.icon!!).getFile(localFile)
                .addOnSuccessListener {
                    val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                    binding.ibMarkerImage.setImageBitmap(bitmap)
                }
        }
        binding.etMarkerName.text.insert(0,marker.title)
        binding.etMarkerAnecdote.text.insert(0,marker.anecdote ?: "")
        // listeners
        binding.ibMarkerImage.setOnClickListener {
            var intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, MY_CAMERA_PERMISSION_CODE)
        }

        binding.btMarkerPicture.setOnClickListener {
            PhoneResources.selectImage(resultLauncher)
        }

        binding.btMarkerCreate.setOnClickListener {
//                .icon(BitmapDescriptorFactory.fromBitmap(image))
            // add to remote
            val fileName = if(imageUri != null ) FirestoreVars.uploadToFireStorage(imageUri!!, onSuccessUpload, onFailureUpload) else null
            val docRef = db.collection(FirestoreVars.collection).document(userEmail!!)
                .collection(FirestoreVars.subColection).document(marker.id!!).set(
                    hashMapOf(
                        "title" to marker.title,
                        "position" to marker.position,
                        "anecdote" to binding.etMarkerAnecdote.text.toString(),
                        "icon" to fileName)
                )
            docRef.addOnSuccessListener { Toast.makeText(this.context,"marker edited successfully!",Toast.LENGTH_LONG).show() }
        }
        binding.btMarkerDelete.setOnClickListener {
            vm.deleteMarker(marker.id!!)
            val action = MarkerEditorFragmentDirections.actionMarkerFragmentToProfileFragment()
            findNavController().navigate(action)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            val image = (data?.extras?.get("data") as Bitmap)
            imageUri = Utils.getImageUriFromBitmap(requireContext(), image)
            binding.ibMarkerImage.setImageURI(imageUri)
        }

    }
}