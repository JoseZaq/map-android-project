package com.example.manec.ui.models

import com.google.android.gms.maps.model.MarkerOptions
import com.google.protobuf.Internal

data class Marker(
    var id: String?=null,
    var icon: String?=null,
    var title: String?=null,
    var anecdote: String?=null,
    var position: Map<String,Double> ?= null
    )