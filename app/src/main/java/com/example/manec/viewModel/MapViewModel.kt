package com.example.manec.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.manec.ui.models.Marker
import com.example.manec.utils.FirestoreVars
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*

class MapViewModel: ViewModel() {
    // vars
    private val db = FirebaseFirestore.getInstance()
    val userEmail = FirebaseAuth.getInstance().currentUser!!.email
    var currentMarker : com.google.android.gms.maps.model.Marker? = null

    private var userMarkers : MutableLiveData<MutableList<Marker>> = MutableLiveData()
    private var markers : MutableLiveData<MutableList<Marker>> = MutableLiveData()

    init {
        updateUserMarkers()
        updateMarkers()
    }
    // methods
    fun setUserMarkers(markerList:MutableList<Marker>) {
        userMarkers.postValue(markerList)
    }

    fun getAllUserMarkers(): MutableLiveData<MutableList<Marker>> {
        return userMarkers
    }

    fun getAllMarkers(): MutableLiveData<MutableList<Marker>> {
        return markers
    }

    fun getUserMarkerByIndex(index:Int): Marker {
        return userMarkers.value!![index]
    }

    fun getMarkerByIndex(index:Int): Marker {
        return markers.value!![index]
    }

    fun getMarkerById(id:String): Marker? {
        return markers.value!!.find { it.id == id }
    }

    fun getMarkerByPosition(position:LatLng): Marker? {
        return markers.value!!.find {
            it.position?.get("latitude") == position.latitude &&
            it.position?.get("longitude") == position.longitude
        }
    }

    fun updateUserMarkers(): MutableLiveData<MutableList<Marker>> {
        db.collection(FirestoreVars.collection).document(userEmail!!)
            .collection(FirestoreVars.subColection)
            .addSnapshotListener(object: EventListener<QuerySnapshot> {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                    if(error != null){
                        Log.e("Firestore error", error.message.toString())
                        return
                    }
                    val markerList  = mutableListOf<Marker>()
                    for(dc: DocumentChange in value?.documentChanges!!){
                        if(dc.type == DocumentChange.Type.ADDED){
                            val newMarker = dc.document.toObject(Marker::class.java)
                            newMarker.id = dc.document.id
                            markerList.add(newMarker)
                        }
                    }
                    userMarkers.postValue(markerList)
                }
            })
        return userMarkers
    }

    fun deleteMarker(markerId: String) {
        db.collection(FirestoreVars.collection).document(userEmail!!)
            .collection(FirestoreVars.subColection).document(markerId)
            .delete()
            .addOnSuccessListener { Log.d("markerDelete", "DocumentSnapshot successfully deleted!") }
            .addOnFailureListener { e -> Log.w("markerDelete", "Error deleting document", e) }
    }

    fun updateMarkers(): MutableLiveData<MutableList<Marker>> {
        db.collectionGroup(FirestoreVars.subColection)
            .addSnapshotListener(object: EventListener<QuerySnapshot> {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                    if(error != null){
                        Log.e("Firestore error", error.message.toString())
                        return
                    }
                    val markerList  = mutableListOf<Marker>()
                    for(dc: DocumentChange in value?.documentChanges!!){
                        if(dc.type == DocumentChange.Type.ADDED){
                            val newMarker = dc.document.toObject(Marker::class.java)
                            newMarker.id = dc.document.id
                            markerList.add(newMarker)
                        }
                    }
                    markers.postValue(markerList)
                }
            })
        return markers
    }

}