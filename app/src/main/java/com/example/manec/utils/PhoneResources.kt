package com.example.manec.utils

import android.content.Intent
import androidx.activity.result.ActivityResultLauncher

class PhoneResources {
    companion object {
        fun selectImage(resultLauncher: ActivityResultLauncher<Intent>) {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            resultLauncher.launch(intent)
        }
    }

}