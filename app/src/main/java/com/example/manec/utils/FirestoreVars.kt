package com.example.manec.utils

import android.net.Uri
import android.widget.Toast
import com.google.firebase.storage.FirebaseStorage
import java.text.SimpleDateFormat
import java.util.*

class FirestoreVars {
    companion object {
        val collection = "users"
        val subColection = "markers"

        fun uploadToFireStorage(imageUri: Uri, onSuccessUpload: () -> Unit , onFailureUpload : () -> Unit): String {
            val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
            val now = Date()
            val fileName = formatter.format(now)
            val storage = FirebaseStorage.getInstance().getReference("images/$fileName")
            storage.putFile(imageUri)
                .addOnSuccessListener {
                    onSuccessUpload()
                }
                .addOnFailureListener {
                    onFailureUpload()
                }
            return storage.path
        }

    }
}