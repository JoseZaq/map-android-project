package com.example.manec

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment


class MainActivity : AppCompatActivity() {
    private var isLogged: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_AnecdoteMap)
        setContentView(R.layout.activity_main)
        //login
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
        val navController = navHostFragment!!.navController
        loadUserData()
        if(!isLogged) {
            navController.navigateUp() // to clear previous navigation history
            navController.navigate(R.id.loginFragment)
        } else {
            navController.navigateUp() // to clear previous navigation history
            navController.navigate(R.id.homeFragment)
        }
    }

    private fun loadUserData() {
        val sharedPreferences = getSharedPreferences("UserCredentials", Context.MODE_PRIVATE)
        val email = sharedPreferences.getString("EMAIL", null)
        val password = sharedPreferences.getString("PASSWORD", null)

        if (email != null && password != null)
            isLogged = true
    }
}